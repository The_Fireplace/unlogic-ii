package the_fireplace.unlogicii.handlers;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.IFuelHandler;
import the_fireplace.unlogicii.UnLogicII;
/**
 * 
 * @author The_Fireplace
 *
 */
public class UnLogicIIFuelHandler implements IFuelHandler {
	private static final int charged_coal_rate = 12800;
	private static final int destabilized_coal_rate = 13000;
	private static final int restabilized_coal_rate = 13400;
	private static final int refined_coal_rate = 16000;
	private static final int screen_rate = 240;
	private static final int wood_tool_rate = 120;
	private int blockMultiplierOf(int i){
		return 9*i + i/10;
	}
	@Override
	public int getBurnTime(ItemStack fuel) {
		Item ifuel = fuel.getItem();
		if(ifuel == UnLogicII.charged_coal){
			return charged_coal_rate;
		}else if(ifuel == UnLogicII.destabilized_coal){
			return destabilized_coal_rate;
		}else if(ifuel == UnLogicII.restabilized_coal){
			return restabilized_coal_rate;
		}else if(ifuel == UnLogicII.refined_coal){
			return refined_coal_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.charged_coal_block)){
			return blockMultiplierOf(charged_coal_rate);
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.black_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.blue_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.brown_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.cyan_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.dark_tan_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.green_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.grey_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.light_orange_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.light_tan_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.lime_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.magenta_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.orange_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.pink_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.purple_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.red_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.silver_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.sky_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.white_screen)){
			return screen_rate;
		}else if(ifuel == Item.getItemFromBlock(UnLogicII.yellow_screen)){
			return screen_rate;
		}else if(ifuel == UnLogicII.wood_paxel){
			return wood_tool_rate;
		}else{
			return 0;
		}
	}
}
