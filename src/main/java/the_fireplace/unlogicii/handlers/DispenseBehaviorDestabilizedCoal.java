package the_fireplace.unlogicii.handlers;

import net.minecraft.dispenser.BehaviorProjectileDispense;
import net.minecraft.dispenser.IPosition;
import net.minecraft.entity.IProjectile;
import net.minecraft.world.World;
import the_fireplace.unlogicii.entity.coal.EntityDestabilizedCoal;

/**
 * @author The_Fireplace
 */
public class DispenseBehaviorDestabilizedCoal extends BehaviorProjectileDispense {
	@Override
	protected IProjectile getProjectileEntity(World worldIn, IPosition position){
		return new EntityDestabilizedCoal(worldIn, position.getX(), position.getY(), position.getZ());
	}
}
