package the_fireplace.unlogicii.compat.basemetals;

import cyano.basemetals.init.Materials;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import the_fireplace.unlogicii.UnLogicII;
import the_fireplace.unlogicii.items.internal.ItemPaxel;

public class RegisterBaseMetals implements IBaseMetalsRegister {
	public static Item copper_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.copper)).setUnlocalizedName("copper_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	public static Item silver_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.silver)).setUnlocalizedName("silver_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	public static Item tin_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.tin)).setUnlocalizedName("tin_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	public static Item lead_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.lead)).setUnlocalizedName("lead_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	public static Item nickel_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.nickel)).setUnlocalizedName("nickel_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	public static Item bronze_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.bronze)).setUnlocalizedName("bronze_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	public static Item brass_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.brass)).setUnlocalizedName("brass_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	public static Item steel_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.steel)).setUnlocalizedName("steel_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	public static Item invar_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.invar)).setUnlocalizedName("invar_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	public static Item electrum_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.electrum)).setUnlocalizedName("electrum_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	public static Item cold_iron_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.coldiron)).setUnlocalizedName("cold_iron_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	public static Item mithril_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.mithril)).setUnlocalizedName("mithril_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	public static Item adamantine_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.adamantine)).setUnlocalizedName("adamantine_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	public static Item star_steel_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.starsteel)).setUnlocalizedName("star_steel_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	public static Item aquarium_paxel = new ItemPaxel(Materials.getToolMaterialFor(Materials.aquarium)).setUnlocalizedName("aquarium_paxel").setCreativeTab(UnLogicII.TabUnLogicII);
	@Override
	public void registerItems() {
		UnLogicII.instance.registerItem(adamantine_paxel);
		UnLogicII.instance.registerItem(aquarium_paxel);
		UnLogicII.instance.registerItem(copper_paxel);
		UnLogicII.instance.registerItem(silver_paxel);
		UnLogicII.instance.registerItem(tin_paxel);
		UnLogicII.instance.registerItem(lead_paxel);
		UnLogicII.instance.registerItem(nickel_paxel);
		UnLogicII.instance.registerItem(bronze_paxel);
		UnLogicII.instance.registerItem(brass_paxel);
		UnLogicII.instance.registerItem(steel_paxel);
		UnLogicII.instance.registerItem(invar_paxel);
		UnLogicII.instance.registerItem(electrum_paxel);
		UnLogicII.instance.registerItem(cold_iron_paxel);
		UnLogicII.instance.registerItem(mithril_paxel);
		UnLogicII.instance.registerItem(star_steel_paxel);
	}
	@Override
	public void registerItemRenders() {
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(copper_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":copper_paxel", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(silver_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":silver_paxel", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(tin_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":tin_paxel", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(lead_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":lead_paxel", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(nickel_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":nickel_paxel", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(bronze_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":bronze_paxel", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(brass_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":brass_paxel", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(steel_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":steel_paxel", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(invar_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":invar_paxel", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(electrum_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":electrum_paxel", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(cold_iron_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":cold_iron_paxel", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(mithril_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":mithril_paxel", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(star_steel_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":star_steel_paxel", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(adamantine_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":adamantine_paxel", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(aquarium_paxel, 0, new ModelResourceLocation(UnLogicII.MODID + ":aquarium_paxel", "inventory"));
	}
}
