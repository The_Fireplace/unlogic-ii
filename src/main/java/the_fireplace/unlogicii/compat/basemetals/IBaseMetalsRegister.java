package the_fireplace.unlogicii.compat.basemetals;

public interface IBaseMetalsRegister {
	void registerItems();
	void registerItemRenders();
}
