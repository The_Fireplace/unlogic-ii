package the_fireplace.unlogicii.libs.tools;

import net.minecraftforge.fml.common.Loader;

/**
 * 
 * @author The_Fireplace
 *
 */
public class MIDLib {
	public static boolean hasBaseMetals(){
		return Loader.isModLoaded("basemetals");
	}
	public static boolean hasPowerAdvantage(){
		return Loader.isModLoaded("poweradvantage");
	}
	public static boolean hasSteelIndustries(){
		return Loader.isModLoaded("SteelIndustries");
	}
	public static boolean hasThaumcraft(){
		return Loader.isModLoaded("Thaumcraft");
	}
	public static boolean hasRealStoneTools(){
		return Loader.isModLoaded("realstonetools");
	}
}
