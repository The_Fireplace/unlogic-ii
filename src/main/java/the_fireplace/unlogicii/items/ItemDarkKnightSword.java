package the_fireplace.unlogicii.items;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import the_fireplace.unlogicii.UnLogicII;
import the_fireplace.unlogicii.entity.living.ExtendedPlayer;
import the_fireplace.unlogicii.network.PacketDispatcher;
import the_fireplace.unlogicii.network.RequestKarmaMessage;
/**
 *
 * @author The_Fireplace
 *
 */
public class ItemDarkKnightSword extends Item {
	public ItemDarkKnightSword(){
		maxStackSize = 1;
		setMaxDamage(512);
		setCreativeTab(UnLogicII.TabUnLogicII);
	}
	@Override
	public boolean hitEntity(ItemStack stack, EntityLivingBase target, EntityLivingBase attacker)
	{
		stack.damageItem(1, attacker);
		if(attacker instanceof EntityPlayer){
			ExtendedPlayer ep = ExtendedPlayer.get((EntityPlayer)attacker);
			if(ep.getKarma() < 0){
				float damage = Math.abs(ep.getKarma())/20;
				target.attackEntityFrom(DamageSource.causePlayerDamage((EntityPlayer)attacker), damage);
				return true;
			}
		}
		return false;
	}
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List tooltip, boolean advanced) {
		PacketDispatcher.sendToServer(new RequestKarmaMessage());
		ExtendedPlayer ep = ExtendedPlayer.get(playerIn);
		tooltip.add("");
		if(ep.getKarma() < 0)
			tooltip.add(EnumChatFormatting.BLUE + "+"+(Math.abs(ep.getKarma())/20)+" Attack Damage");
		else
			tooltip.add(EnumChatFormatting.BLUE + "+0 Attack Damage");
	}
	@Override
	@SideOnly(Side.CLIENT)
	public boolean isFull3D()
	{
		return true;
	}
	@Override
	public EnumAction getItemUseAction(ItemStack stack)
	{
		return EnumAction.BLOCK;
	}
	@Override
	public int getMaxItemUseDuration(ItemStack stack)
	{
		return 72000;
	}
	@Override
	public ItemStack onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn)
	{
		playerIn.setItemInUse(itemStackIn, this.getMaxItemUseDuration(itemStackIn));
		return itemStackIn;
	}
	@Override
	public boolean canHarvestBlock(Block blockIn)
	{
		return blockIn == Blocks.web;
	}
	@Override
	public int getItemEnchantability()
	{
		return 30;
	}
	@Override
	public boolean getIsRepairable(ItemStack toRepair, ItemStack repair) {
		ItemStack mat = new ItemStack(UnLogicII.soul_negative);
		return mat != null && net.minecraftforge.oredict.OreDictionary.itemMatches(mat, repair, false) || super.getIsRepairable(toRepair, repair);
	}
}
