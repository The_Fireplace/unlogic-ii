package the_fireplace.unlogicii.items;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import the_fireplace.unlogicii.UnLogicII;
import the_fireplace.unlogicii.entity.living.ExtendedPlayer;
import the_fireplace.unlogicii.enums.EnumAmmo;
/**
 * 
 * @author The_Fireplace
 *
 */
public class ItemBlockSmartCoalGun extends ItemBlock {

	public ItemBlockSmartCoalGun(Block b) {
		super(b);
		setMaxStackSize(1);
		setUnlocalizedName("smart_coal_gun");
	}
	@Override
	public ItemStack onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn)
	{//shoots the coal type the user is currently on, and switches to the next available ammo if the player is sing the wrong type
		EnumAmmo ammo = ExtendedPlayer.get(playerIn).getAmmoType();
		while(!playerIn.inventory.hasItem(ammo.toItem()) &&  (playerIn.inventory.hasItem(Items.coal) || playerIn.inventory.hasItem(UnLogicII.charged_coal) || playerIn.inventory.hasItem(UnLogicII.destabilized_coal) || playerIn.inventory.hasItem(UnLogicII.restabilized_coal) || playerIn.inventory.hasItem(UnLogicII.refined_coal))){
			ExtendedPlayer.get(playerIn).setAmmo(ammo.next());
			ammo = ExtendedPlayer.get(playerIn).getAmmoType();//setting again to ensure that it updates
		}
		if(playerIn.inventory.hasItem(ammo.toItem())){
			if(!worldIn.isRemote){
				worldIn.spawnEntityInWorld(ammo.makeEntity(worldIn, playerIn));
			}
			playerIn.inventory.consumeInventoryItem(ammo.toItem());
		}
		return itemStackIn;
	}
	@Override
	public boolean hitEntity(ItemStack stack, EntityLivingBase target, EntityLivingBase attacker)
	{
		if(((EntityPlayer)attacker).capabilities.isCreativeMode || ((EntityPlayer)attacker).inventory.consumeInventoryItem(Items.gunpowder)){
			if(((EntityPlayer)attacker).capabilities.isCreativeMode || ((EntityPlayer)attacker).inventory.consumeInventoryItem(Items.gunpowder)){
				target.worldObj.createExplosion(null, (attacker.posX + target.posX)/2, (attacker.posY + target.posY)/2, (attacker.posZ + target.posZ)/2, 2, false);
				return true;
			}else{
				target.worldObj.createExplosion(null, (attacker.posX + target.posX)/2, (attacker.posY + target.posY)/2, (attacker.posZ + target.posZ)/2, 1, false);
				return true;
			}
		}
		return false;
	}
}
