package the_fireplace.unlogicii.items;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import the_fireplace.unlogicii.entity.living.ExtendedPlayer;
import the_fireplace.unlogicii.enums.EnumAmmo;
/**
 * 
 * @author The_Fireplace
 *
 */
public class ItemBlockCoalGun extends ItemBlock {

	public ItemBlockCoalGun(Block b) {
		super(b);
		setMaxStackSize(1);
		setUnlocalizedName("coal_gun");
	}
	@Override
	public ItemStack onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn)
	{//shoots the coal type the user is currently on
		EnumAmmo ammo = ExtendedPlayer.get(playerIn).getAmmoType();

		if(playerIn.inventory.hasItem(ammo.toItem())){
			if(!worldIn.isRemote){
				worldIn.spawnEntityInWorld(ammo.makeEntity(worldIn, playerIn));
			}
			playerIn.inventory.consumeInventoryItem(ammo.toItem());
		}

		return itemStackIn;
	}
	@Override
	public boolean hitEntity(ItemStack stack, EntityLivingBase target, EntityLivingBase attacker)
	{
		if(((EntityPlayer)attacker).capabilities.isCreativeMode || ((EntityPlayer)attacker).inventory.consumeInventoryItem(Items.gunpowder)){
			if(((EntityPlayer)attacker).capabilities.isCreativeMode || ((EntityPlayer)attacker).inventory.consumeInventoryItem(Items.gunpowder)){
				target.worldObj.createExplosion(null, (attacker.posX + target.posX)/2, (attacker.posY + target.posY)/2, (attacker.posZ + target.posZ)/2, 2, false);
				return true;
			}else{
				target.worldObj.createExplosion(null, (attacker.posX + target.posX)/2, (attacker.posY + target.posY)/2, (attacker.posZ + target.posZ)/2, 1, false);
				return true;
			}
		}
		return false;
	}
}
