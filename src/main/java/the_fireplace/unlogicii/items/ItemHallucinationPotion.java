package the_fireplace.unlogicii.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.stats.StatList;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import the_fireplace.unlogicii.UnLogicII;
import the_fireplace.unlogicii.entity.EntityHallucinationPotion;

import java.util.List;

/**
 * @author The_Fireplace
 */
public class ItemHallucinationPotion extends Item {
	public ItemHallucinationPotion(){
		super();
		setCreativeTab(UnLogicII.TabUnLogicII);
		setHasSubtypes(true);
		setMaxStackSize(1);
		setUnlocalizedName("hallucination_potion");
	}
	@Override
	public ItemStack onItemUseFinish(ItemStack stack, World worldIn, EntityPlayer playerIn) {
		if (!playerIn.capabilities.isCreativeMode) {
			--stack.stackSize;
		}
		if (!worldIn.isRemote){
			switch(stack.getItemDamage()){
				case 0:
					playerIn.addPotionEffect(new PotionEffect(UnLogicII.hallucination.id, 3600));
				case 1:
					playerIn.addPotionEffect(new PotionEffect(UnLogicII.hallucination.id, 9600));
				default:
					break;
			}
		}
		playerIn.triggerAchievement(StatList.objectUseStats[Item.getIdFromItem(this)]);

		if (!playerIn.capabilities.isCreativeMode)
		{
			if (stack.stackSize <= 0)
			{
				return new ItemStack(Items.glass_bottle);
			}

			playerIn.inventory.addItemStackToInventory(new ItemStack(Items.glass_bottle));
		}

		return stack;
	}
	@Override
	@SideOnly(Side.CLIENT)
	public boolean hasEffect(ItemStack stack)
	{
		return true;
	}
	@Override
	public int getMaxItemUseDuration(ItemStack stack)
	{
		return 32;
	}
	@Override
	public EnumAction getItemUseAction(ItemStack stack)
	{
		return EnumAction.DRINK;
	}
	@Override
	public ItemStack onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn)
	{
		if (itemStackIn.getMetadata() > 1)
		{
			if (!playerIn.capabilities.isCreativeMode)
			{
				--itemStackIn.stackSize;
			}

			worldIn.playSoundAtEntity(playerIn, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

			if (!worldIn.isRemote)
			{
				worldIn.spawnEntityInWorld(new EntityHallucinationPotion(worldIn, playerIn, itemStackIn));
			}

			playerIn.triggerAchievement(StatList.objectUseStats[Item.getIdFromItem(this)]);
			return itemStackIn;
		}else{
			playerIn.setItemInUse(itemStackIn, this.getMaxItemUseDuration(itemStackIn));
			return itemStackIn;
		}
	}
	@Override
	public String getItemStackDisplayName(ItemStack stack)
	{
		String s = "";

		if (stack.getItemDamage() > 1)
		{
			s = StatCollector.translateToLocal("potion.prefix.grenade").trim() + " ";
		}

		return s+StatCollector.translateToLocal("item.hallucination_potion.name");
	}
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List<String> tooltip, boolean advanced)
	{
		String s1=StatCollector.translateToLocal("potion.hallucination");
		if(stack.getItemDamage() == 0)
			s1 += " (3:00)";
		else if(stack.getItemDamage() == 1)
			s1 += " (8:00)";
		else if(stack.getItemDamage() == 2)
			s1 += " (2:15)";
		else if(stack.getItemDamage() == 3)
			s1 += " (6:00)";
		tooltip.add(EnumChatFormatting.RED + s1);
	}
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item itemIn, CreativeTabs tab, List<ItemStack> subItems)
	{
		subItems.add(new ItemStack(itemIn, 1, 0));
		subItems.add(new ItemStack(itemIn, 1, 1));
		subItems.add(new ItemStack(itemIn, 1, 2));
		subItems.add(new ItemStack(itemIn, 1, 3));
	}
}
