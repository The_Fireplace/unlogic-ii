package the_fireplace.unlogicii.items;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import the_fireplace.unlogicii.UnLogicII;
/**
 * 
 * @author The_Fireplace
 *
 */
public class ItemChargedCoal extends Item {
	public ItemChargedCoal(){
		setUnlocalizedName("charged_coal");
		setCreativeTab(UnLogicII.TabUnLogicII);
	}

	@Override
	public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if(world.getBlockState(pos).getBlock() == Blocks.redstone_ore || world.getBlockState(pos).getBlock() == Blocks.lit_redstone_ore){
			world.setBlockToAir(pos);
			world.setBlockState(pos, Blocks.coal_ore.getDefaultState());
			--stack.stackSize;
			return true;
		}
		return false;
	}
}
