package the_fireplace.unlogicii.config;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.config.GuiConfig;
import the_fireplace.unlogicii.UnLogicII;
/**
 * 
 * @author The_Fireplace
 *
 */
public class UnLogicIIConfigGui extends GuiConfig {

	public UnLogicIIConfigGui(GuiScreen parentScreen) {
		super(parentScreen, new ConfigElement(UnLogicII.config.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(), UnLogicII.MODID, false,
				false, GuiConfig.getAbridgedConfigPath(UnLogicII.config.toString()));
	}

}
