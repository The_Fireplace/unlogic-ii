package the_fireplace.unlogicii.entity.tile;

import net.minecraft.tileentity.TileEntity;
/**
 * 
 * @author The_Fireplace
 *
 */
public class TileEntityCoalGun extends TileEntity {
	public static final String publicName="tileEntityCoalGun";
	private String name="tileEntityCoalGun";

	public String getName() {
		return name;
	}
}
