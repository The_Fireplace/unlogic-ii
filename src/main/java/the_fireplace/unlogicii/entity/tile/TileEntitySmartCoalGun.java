package the_fireplace.unlogicii.entity.tile;

import net.minecraft.tileentity.TileEntity;
/**
 * 
 * @author The_Fireplace
 *
 */
public class TileEntitySmartCoalGun extends TileEntity {
	public static final String publicName="tileEntitySmartCoalGun";
	private String name="tileEntitySmartCoalGun";

	public String getName() {
		return name;
	}
}
