package the_fireplace.unlogicii.entity.living;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityIronGolem;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;

/**
 * 
 * @author The_Fireplace
 *
 */
public class ExtendedLivingBase implements IExtendedEntityProperties {
	public final static String EXT_PROP_NAME = "UnLogicIIExtendedLiving";
	private final EntityLivingBase entity;
	private byte soultype = 0;
	private boolean isInfected = false;

	public ExtendedLivingBase(EntityLivingBase entity){
		this.entity = entity;
		if(this.entity.getClass().isAssignableFrom(IMob.class)){
			soultype=-1;
		}
		if(this.entity.getClass().isAssignableFrom(IAnimals.class)){
			soultype=0;
		}
		if(this.entity.getClass().equals(EntityIronGolem.class) || this.entity.getClass().equals(EntityVillager.class) || this.entity.getClass().isAssignableFrom(EntityVillager.class)){
			soultype=1;
		}
	}

	public static final void register(EntityLivingBase entity){
		entity.registerExtendedProperties(EXT_PROP_NAME, new ExtendedLivingBase(entity));
	}

	public static final ExtendedLivingBase get(EntityLivingBase entity){
		return (ExtendedLivingBase)entity.getExtendedProperties(EXT_PROP_NAME);
	}

	@Override
	public void saveNBTData(NBTTagCompound compound) {
		compound.setByte("SoulType", soultype);
		compound.setBoolean("IsInfected", isInfected);
	}

	@Override
	public void loadNBTData(NBTTagCompound compound) {
		this.soultype = compound.getByte("SoulType");
		this.isInfected = compound.getBoolean("IsInfected");
	}

	@Override
	public void init(Entity entity, World world) {
	}

	public boolean getIsInfected(){
		return isInfected;
	}

	public void setIsInfected(boolean isInfected){
		if(!this.isInfected && isInfected){
			//give entity infected ai
		}
		if(this.isInfected && !isInfected){
			//remove infected ai from entity
		}
		this.isInfected = isInfected;
	}

	public byte getSoulType(){
		return soultype;
	}

	public void setSoulType(byte type){
		this.soultype = type;
	}
}
