package the_fireplace.unlogicii.blocks;

import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import the_fireplace.unlogicii.UnLogicII;
/**
 *
 * @author The_Fireplace
 *
 */
public class BlockObsidianFarmland extends ULBlock {

	public BlockObsidianFarmland() {
		super(Material.rock);
		setUnlocalizedName("obsidian_farmland");
		setHarvestLevel("pickaxe", 3);
		setHardness(50.0F);
		setResistance(2000.0F);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Item getItem(World worldIn, BlockPos pos)
	{
		return Item.getItemFromBlock(Blocks.obsidian);
	}

	@Override
	public boolean canSilkHarvest(World world, BlockPos pos, IBlockState state, EntityPlayer player)
	{
		return true;
	}

	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
	{
		return Item.getItemFromBlock(Blocks.obsidian);
	}

	@Override
	public boolean canPlaceTorchOnTop(IBlockAccess world, BlockPos pos)
	{
		return false;
	}

	@Override
	public boolean canSustainPlant(IBlockAccess world, BlockPos pos, EnumFacing direction, IPlantable plantable)
	{
		int i = 0;
		while(i <= 5){
			if(plantable.getPlant(world, pos) == UnLogicII.quartz_crop.getStateFromMeta(i)){
				i = i+1;
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isFertile(World world, BlockPos pos)
	{
		return true;
	}
}
