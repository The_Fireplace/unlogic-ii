package the_fireplace.unlogicii.blocks;

import net.minecraft.block.material.Material;
/**
 *
 * @author The_Fireplace
 *
 */
public class BlockCompactDirt extends ULBlock {

	public BlockCompactDirt() {
		super(Material.ground);
		setHardness(2.3F);
		setUnlocalizedName("compact_dirt");
		setHarvestLevel("shovel", 1);
	}

}
