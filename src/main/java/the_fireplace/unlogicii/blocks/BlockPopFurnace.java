package the_fireplace.unlogicii.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumWorldBlockLayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.internal.FMLNetworkHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import the_fireplace.unlogicii.UnLogicII;
import the_fireplace.unlogicii.entity.tile.TileEntityPopFurnace;
import the_fireplace.unlogicii.libs.tools.MiscTools;
/**
 *
 * @author The_Fireplace
 *
 */
public class BlockPopFurnace extends BlockContainer {
	public static final PropertyDirection FACING = PropertyDirection.create("facing", EnumFacing.Plane.HORIZONTAL);

	public BlockPopFurnace() {
		super(Material.iron);
		setUnlocalizedName("pop_furnace");
		setCreativeTab(UnLogicII.TabUnLogicII);
		setHardness(5F);
		setResistance(15F);
		setHarvestLevel("pickaxe", 0);
		this.setDefaultState(this.blockState.getBaseState().withProperty(FACING, EnumFacing.NORTH));
	}
	@Override
	public boolean isOpaqueCube(){
		return true;
	}

	@Override
	public boolean isVisuallyOpaque(){
		return true;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumWorldBlockLayer getBlockLayer(){
		return EnumWorldBlockLayer.SOLID;
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEntityPopFurnace();
	}

	@Override
	public void onBlockAdded(World world, BlockPos pos, IBlockState state){
		super.onBlockAdded(world, pos, state);
		setDefaultDirection(world, pos, state);
	}

	private void setDefaultDirection(World worldIn, BlockPos pos, IBlockState state){
		if(!worldIn.isRemote){
			Block block = worldIn.getBlockState(pos.north()).getBlock();
			Block block1 = worldIn.getBlockState(pos.south()).getBlock();
			Block block2 = worldIn.getBlockState(pos.west()).getBlock();
			Block block3 = worldIn.getBlockState(pos.east()).getBlock();
			EnumFacing enumfacing = state.getValue(FACING);

			if (enumfacing == EnumFacing.NORTH && block.isFullBlock() && !block1.isFullBlock())
				enumfacing = EnumFacing.SOUTH;
			else if (enumfacing == EnumFacing.SOUTH && block1.isFullBlock() && !block.isFullBlock())
				enumfacing = EnumFacing.NORTH;
			else if (enumfacing == EnumFacing.WEST && block2.isFullBlock() && !block3.isFullBlock())
				enumfacing = EnumFacing.EAST;
			else if (enumfacing == EnumFacing.EAST && block3.isFullBlock() && !block2.isFullBlock())
				enumfacing = EnumFacing.WEST;

			worldIn.setBlockState(pos, state.withProperty(FACING, enumfacing), 2);
		}
	}

	@Override
	public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
	{
		return this.getDefaultState().withProperty(FACING, placer.getHorizontalFacing().getOpposite());
	}

	@Override
	public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
	{
		worldIn.setBlockState(pos, state.withProperty(FACING, placer.getHorizontalFacing().getOpposite()), 2);
	}

	@Override
	public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if(worldIn.isRemote)
			return true;
		else if(!playerIn.isSneaking()){
			TileEntity tileentity = worldIn.getTileEntity(pos);

			if (tileentity instanceof TileEntityPopFurnace)
			{
				FMLNetworkHandler.openGui(playerIn, UnLogicII.MODID, 0, worldIn, pos.getX(), pos.getY(), pos.getZ());
			}
			return true;
		}else
			return false;
	}

	@Override
	protected BlockState createBlockState(){
		return new BlockState(this, FACING);
	}

	@Override
	public IBlockState getStateFromMeta(int meta)
	{
		EnumFacing enumfacing = EnumFacing.getFront(meta);

		if (enumfacing.getAxis() == EnumFacing.Axis.Y)
			enumfacing = EnumFacing.NORTH;

		return this.getDefaultState().withProperty(FACING, enumfacing);
	}
	@Override
	public int getMetaFromState(IBlockState state){
		return state.getValue(FACING).getIndex();
	}
	@Override
	public int getRenderType(){
		return 3;
	}
	@Override
	public void onNeighborBlockChange(World worldIn, BlockPos pos, IBlockState state, Block neighborBlock){
		if (!worldIn.isRemote)
			if (worldIn.isBlockPowered(pos))
				popItems(worldIn, pos);
	}
	@Override
	public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random rand){
		if (!worldIn.isRemote)
			if (worldIn.isBlockPowered(pos))
				popItems(worldIn, pos);
	}
	public void popItems(World worldIn, BlockPos pos){
		TileEntity te = worldIn.getTileEntity(pos);
		if(te instanceof TileEntityPopFurnace)
			((TileEntityPopFurnace) te).popItems();
	}
	@Override
	public void breakBlock(World worldIn, BlockPos pos, IBlockState state){
		TileEntityPopFurnace tile = (TileEntityPopFurnace)worldIn.getTileEntity(pos);
		int g = tile.getStoredGunpowder();
		int f = tile.getStoredFirestarter();
		int gstacks = 0;
		int fstacks = 0;
		while(g>64){
			gstacks++;
			g -= 64;
		}
		while(f>64){
			fstacks++;
			f -= 64;
		}
		while(gstacks > 0){
			MiscTools.spawnItemAtPos(new ItemStack(UnLogicII.gunpowder_substitute, 64), worldIn, pos);
			gstacks--;
		}
		while(fstacks > 0){
			MiscTools.spawnItemAtPos(new ItemStack(UnLogicII.firestarter_substitute, 64), worldIn, pos);
			fstacks--;
		}
		if(g > 0)
			MiscTools.spawnItemAtPos(new ItemStack(UnLogicII.gunpowder_substitute, g), worldIn, pos);
		if(f > 0)
			MiscTools.spawnItemAtPos(new ItemStack(UnLogicII.firestarter_substitute, f), worldIn, pos);
		super.breakBlock(worldIn, pos, state);
	}
}
