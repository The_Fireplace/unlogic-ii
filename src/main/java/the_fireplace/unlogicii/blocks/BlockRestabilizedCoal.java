package the_fireplace.unlogicii.blocks;

import net.minecraft.block.material.Material;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.IBlockAccess;

/**
 * @author The_Fireplace
 */
public class BlockRestabilizedCoal extends ULBlock {
	public BlockRestabilizedCoal() {
		super(Material.rock);
		setUnlocalizedName("restabilized_coal_block");
		setHardness(5.0F);
		setResistance(10.0F);
		setHarvestLevel("pickaxe", 0);
	}

	@Override
	public int getFlammability(IBlockAccess world, BlockPos pos, EnumFacing face)
	{
		return 20;
	}
}
