package the_fireplace.unlogicii.blocks.internal;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import the_fireplace.unlogicii.UnLogicII;

import java.util.Random;
/**
 * 
 * @author The_Fireplace
 * 
 */
public class BlockQuartzCrop extends Block5Crops{

	public BlockQuartzCrop(){
		this.setTickRandomly(true);
		float f = 0.5F;
		this.setBlockBounds(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, 0.25F, 0.5F + f);
		setUnlocalizedName("quartz_crop");
		this.setHardness(0.0F);
		this.setStepSound(soundTypeGrass);
		this.disableStats();
	}

	@Override
	public Item getSeed(){
		return UnLogicII.quartz_seeds;
	}

	@Override
	public Item getCrop(){
		return Item.getItemFromBlock(Blocks.quartz_block);
	}

	@Override
	protected boolean canPlaceBlockOn(Block ground)
	{
		return ground == UnLogicII.obsidian_farmland;
	}

	@Override
	public void grow(World worldIn, BlockPos pos, IBlockState state)
	{
		int i = state.getValue(AGE) + MathHelper.getRandomIntegerInRange(worldIn.rand, 2, 5);

		if (i > 5)
		{
			i = 5;
		}

		worldIn.setBlockState(pos, state.withProperty(AGE, i), 2);
	}

	@Override
	public boolean canBlockStay(World worldIn, BlockPos pos, IBlockState state)
	{
		return (worldIn.getBlockState(pos.down()).getBlock().canSustainPlant(worldIn, pos.down(), net.minecraft.util.EnumFacing.UP, this));
	}

	@Override
	public Item getItemDropped(IBlockState state, Random rand, int fortune)
	{
		return state.getValue(AGE) == 5 ? this.getCrop() : this.getSeed();
	}

	@Override
	public boolean canGrow(World worldIn, BlockPos pos, IBlockState state, boolean isClient)
	{
		return state.getValue(AGE) < 5;
	}

	@Override
	public java.util.List<ItemStack> getDrops(net.minecraft.world.IBlockAccess world, BlockPos pos, IBlockState state, int fortune)
	{
		java.util.List<ItemStack> ret = super.getDrops(world, pos, state, fortune);
		int age = state.getValue(AGE);
		Random rand = world instanceof World ? ((World)world).rand : new Random();

		if (age >= 5)
		{
			for (int i = 0; i < 3 + fortune; ++i)
			{
				if (rand.nextInt(15) <= age)
				{
					ret.add(new ItemStack(this.getSeed(), 1, 0));
				}
			}
		}

		if (rand.nextInt(1) == 0)
			ret.add(new ItemStack(UnLogicII.quartz_seeds));
		return ret;
	}

	@Override
	public int quantityDropped(Random random)
	{
		return 3;
	}
}
