package the_fireplace.unlogicii.blocks;

import net.minecraft.block.material.Material;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumWorldBlockLayer;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
/**
 *
 * @author The_Fireplace
 *
 */
public class BlockUnLogicGemNegative extends ULBlock {

	public BlockUnLogicGemNegative() {
		super(Material.iron);
		setUnlocalizedName("unlogic_gem_block_negative");
		setLightOpacity(14);
		setLightLevel(3);
		setHarvestLevel("pickaxe", 2);
		setHardness(5.0F);
		setResistance(10.0F);
	}

	@Override
	public boolean isVisuallyOpaque()
	{
		return false;
	}

	@Override
	public boolean isOpaqueCube(){
		return false;
	}

	@Override
	public int getFireSpreadSpeed(IBlockAccess world, BlockPos pos, EnumFacing face)
	{
		return -10;
	}

	@Override
	public boolean isBeaconBase(IBlockAccess worldObj, BlockPos pos, BlockPos beacon)
	{
		return true;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumWorldBlockLayer getBlockLayer(){
		return EnumWorldBlockLayer.TRANSLUCENT;
	}
}
