package the_fireplace.unlogicii.blocks;

import net.minecraft.block.material.Material;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
/**
 *
 * @author The_Fireplace
 *
 */
public class BlockFireplaceBottom extends ULBlock {

	public BlockFireplaceBottom() {
		super(Material.rock);
		setUnlocalizedName("fireplace_bottom");
		setHardness(2.0F);
		setResistance(10.0F);
		setHarvestLevel("pickaxe", 0);
	}
	@Override
	public boolean isFireSource(World world, BlockPos pos, EnumFacing side)
	{
		return true;
	}
}
