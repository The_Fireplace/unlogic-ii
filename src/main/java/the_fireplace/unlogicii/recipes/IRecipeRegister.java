package the_fireplace.unlogicii.recipes;
/**
 * 
 * @author The_Fireplace
 *
 */
public interface IRecipeRegister {
	void registerRecipes();
}
