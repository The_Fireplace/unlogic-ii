package the_fireplace.unlogicii.recipes;

/**
 * 
 * @author The_Fireplace
 *
 */
public class SteelIndustriesRecipes extends VanillaStacks implements IRecipeRegister {

	@Override
	public void registerRecipes() {
		shaped(darkKnightSwordStack, " gt", "gtg", "sg ", 'g', unlogicGemNegativeStack, 't', "ingotTitanium", 's', "stickWood");
		shaped(paladinSwordStack, " gt", "gtg", "sg ", 'g', unlogicGemPositiveStack, 't', "ingotTitanium", 's', "stickWood");
	}
}
