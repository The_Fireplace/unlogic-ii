package the_fireplace.unlogicii;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
/**
 *
 * @author The_Fireplace
 *
 */
public class TabUnLogicII extends CreativeTabs {
	public TabUnLogicII() {
		super("unlogicii");
	}

	@Override
	public Item getTabIconItem() {
		return Item.getItemFromBlock(UnLogicII.shell_core);
	}
}
