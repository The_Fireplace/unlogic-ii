package the_fireplace.unlogicii.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import the_fireplace.unlogicii.entity.living.ExtendedPlayer;

public class KarmaMessage implements IMessage {
	public long karma;
	public KarmaMessage(){}

	public KarmaMessage(long karma){
		this.karma = karma;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		karma = Long.parseLong(ByteBufUtils.readUTF8String(buf));
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, String.valueOf(karma));
	}

	public static class Handler extends AbstractClientMessageHandler<KarmaMessage>{
		@Override
		public IMessage handleClientMessage(EntityPlayer player, KarmaMessage message, MessageContext ctx) {
			ExtendedPlayer.get(player).setKarmaTo(message.karma);
			return null;
		}
	}
}
