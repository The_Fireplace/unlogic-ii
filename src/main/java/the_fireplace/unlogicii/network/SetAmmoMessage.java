package the_fireplace.unlogicii.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import the_fireplace.unlogicii.entity.living.ExtendedPlayer;
import the_fireplace.unlogicii.enums.EnumAmmo;

/**
 * 
 * @author The_Fireplace
 *
 */
public class SetAmmoMessage implements IMessage {
	public EnumAmmo ammo;
	public SetAmmoMessage(){}

	public SetAmmoMessage(EnumAmmo ammo){
		this.ammo = ammo;
	}
	@Override
	public void fromBytes(ByteBuf buf) {
		this.ammo = EnumAmmo.getAmmoFromString(ByteBufUtils.readUTF8String(buf));
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, ammo.toString());
	}
	public static class Handler extends AbstractClientMessageHandler<SetAmmoMessage>{
		@Override
		public IMessage handleClientMessage(EntityPlayer player, SetAmmoMessage message, MessageContext ctx) {
			ExtendedPlayer.get(player).setAmmo(message.ammo);
			return null;
		}
	}
}
