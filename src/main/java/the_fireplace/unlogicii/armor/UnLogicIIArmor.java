package the_fireplace.unlogicii.armor;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import the_fireplace.unlogicii.UnLogicII;
import the_fireplace.unlogicii.enums.EnumArmorType;

import java.util.List;
/**
 * 
 * @author The_Fireplace
 *
 */
public class UnLogicIIArmor extends ItemArmor
{
	public UnLogicIIArmor(ArmorMaterial material, EnumArmorType armortype) {
		super(material, -1, armortype.getIndex());
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
		if(stack.getItem() == UnLogicII.crystal_eye_headband)
			return "unlogicii:textures/armor/crystal_eye_headband.png";
		if(stack.getItem() == UnLogicII.hallucination_goggles)
			return "unlogicii:textures/armor/hallucination_goggles.png";
		return null;
	}
	@Override
	@SideOnly(Side.CLIENT)
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List tooltip, boolean advanced) {
		if(stack.getItem() == UnLogicII.crystal_eye_headband)
			tooltip.add(StatCollector.translateToLocal("unlogiciiarmor.crystal_eye_tooltip"));
		else
			super.addInformation(stack, playerIn, tooltip, advanced);
	}
	@Override
	public void onArmorTick(World world, EntityPlayer player, ItemStack is){
		if(player.getCurrentArmor(3).getItem() == UnLogicII.hallucination_goggles)
			player.addPotionEffect(new PotionEffect(UnLogicII.hallucination.getId(), 20, 0, true, true));
		super.onArmorTick(world, player, is);
	}
}


